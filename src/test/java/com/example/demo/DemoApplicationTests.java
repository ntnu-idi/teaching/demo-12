package com.example.demo;

import org.junit.jupiter.api.Test;
//import org.junit.jupiter.api.Disable;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit4.SpringRunner;
import static org.assertj.core.api.Assertions.assertThat;

public class DemoApplicationTests {


	@Test
	public void addTest() {
		//String body = this.restTemplate.getForObject("/", String.class);
		//assertThat(body).isEqualTo("Spring is here!");

		int expectedResult = 5;
		int receivedResult = Calculator.add(2,3);

		assertThat(expectedResult).isEqualTo(receivedResult);
	}


	//@Disable
	@Test
	public void multplyTest() {
		//String body = this.restTemplate.getForObject("/", String.class);
		//assertThat(body).isEqualTo("Spring is here!");

		int expectedResult = 6;
		int receivedResult = Calculator.multiply(2,3);

		assertThat(expectedResult).isEqualTo(receivedResult);
	}



	//@Test
	public void devideTest() {

		int expectedResult = 10;
		int receivedResult = Calculator.devide(20,2);

		assertThat(expectedResult).isEqualTo(receivedResult);
	}

}